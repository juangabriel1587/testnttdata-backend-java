package com.microservicioclientes.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.microservicioclientes.dto.ClienteDto;
import com.microservicioclientes.dto.CuentaDto;
import com.microservicioclientes.services.ICuentaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(description = "Gestiona ciclo de vida de cuentas")
public class CuentaRestController {

	@Autowired
	private ICuentaService cuentaService;
	
	
	@GetMapping(value = "/cuentas")
	@ApiOperation(value = "Lista de cuentas")
	public ResponseEntity<?> index() throws Exception{
		
		Map<String, Object> response = new HashMap<>();
		try {
			List<CuentaDto> cuentassLis = cuentaService.findAll();
			return new ResponseEntity<List<CuentaDto>>(cuentassLis,HttpStatus.OK);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al consultar cuentas");
			response.put("error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/cuentas/{id}")
	@ApiOperation(value = "Obtiene cuenta por id")
	public ResponseEntity<?> show(@PathVariable Long id)throws Exception {
		Map<String, Object> response = new HashMap<>();
		try {
			CuentaDto cuenta = cuentaService.finfById(id);
			if(cuenta==null) {
				response.put("messagge", "Cliente con ID: ".concat(id.toString().concat(" NO existe en la base de datos")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<CuentaDto>(cuenta,HttpStatus.OK);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al obtener cuanta");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	
	@GetMapping(value = "/cuentas/cliente/{clienteId}")
	@ApiOperation(value = "Lista de cuentas de cliente")
	public ResponseEntity<?> showByClienteId(@PathVariable Long clienteId) throws Exception{
		
		Map<String, Object> response = new HashMap<>();
		try {
			List<CuentaDto> cuentassLis = cuentaService.finByClienteId(clienteId);
			return new ResponseEntity<List<CuentaDto>>(cuentassLis,HttpStatus.OK);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al consultar cuentas");
			response.put("error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/cuentas")
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiOperation(value = "Crear nueva cuenta")
	public ResponseEntity<?> create(@Valid @RequestBody CuentaDto cuenta, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		try {
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
			}
			
			CuentaDto cuentaN = cuentaService.save(cuenta);
			return new ResponseEntity<CuentaDto>(cuentaN,HttpStatus.CREATED);
		} catch (DataAccessException e) {
			response.put("messagge", "Error al insertar en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		//return clienteService.save(cliente);
	}
	
	
	@PutMapping("/cuentas/{id}")
	@ApiOperation(value = "Actualizar cliente por ID")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<?> update(@Valid @RequestBody CuentaDto cuentaUpdate ,@PathVariable Long id,BindingResult result){
		Map<String, Object> response = new HashMap<>();
		if(id==null) {
			response.put("messagge", "ID de cliente  es requerido");
			return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		try {
			CuentaDto cuenta = cuentaService.finfById(id);
			if(cuenta==null) {
				response.put("messagge", "Cuenta con ID: ".concat(id.toString().concat(" NO existe en la base de datos")));
				return new ResponseEntity<Map<String ,Object>>(response,HttpStatus.NOT_FOUND);
			}
			if (result.hasErrors()) {
				List<String> errors = result.getFieldErrors().stream()
						.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
						.collect(Collectors.toList());
				response.put("errors", errors);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
			
			}
			
			cuenta.setEstado(cuentaUpdate.getEstado());
			cuenta.setNumeroCuenta(cuentaUpdate.getNumeroCuenta());
			cuenta.setSaldoInicial(cuentaUpdate.getSaldoInicial());
			cuenta.setTipoCuenta(cuentaUpdate.getTipoCuenta());
			//cuenta.set
			
			CuentaDto cuentaUp = cuentaService.save(cuenta);
			
			return new ResponseEntity<CuentaDto>(cuentaUp,HttpStatus.CREATED);
			
			
			
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("messagge", "Error al insertar en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

		
	}
	
	
	@DeleteMapping("/cuentas/{id}")
	@ApiOperation(value = "Borrar cliente por ID")
	public  ResponseEntity<?> delete(@PathVariable Long id)
	{
		Map<String, Object> response = new HashMap<>();
		try {
			cuentaService.delete(id);
			response.put("mensaje", "Cuenta eliminado con éxito!");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar cueta de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
