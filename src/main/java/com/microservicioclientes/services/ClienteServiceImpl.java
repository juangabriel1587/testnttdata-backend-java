package com.microservicioclientes.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.microservicioclientes.dto.ClienteDto;
import com.microservicioclientes.models.dao.IClienteDao;
import com.microservicioclientes.models.entity.Cliente;


@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteDao clienteDao;

	@Autowired
	private ModelMapper modelMapper;

	
	
	
	@Override
	@Transactional(readOnly = true)
	public List<ClienteDto> findAll() {
		// TODO Auto-generated method stub
		List<Cliente> personas = clienteDao.findAll();
		return personas.size()>0? personas.stream().map(cliente -> modelMapper.map(cliente, ClienteDto.class))
				.collect(Collectors.toList()):null;
	}

	@Override
	@Transactional
	public ClienteDto save(ClienteDto cliente) {
		// TODO Auto-generated method stub
		Cliente personaN = clienteDao.save(modelMapper.map(cliente, Cliente.class));
		return personaN==null? null: modelMapper.map(personaN, ClienteDto.class);
	}

	@Override
	@Transactional(readOnly = true)
	public ClienteDto findById(Long id) {
		// TODO Auto-generated method stub
		Cliente cliente = clienteDao.findById(id).orElse(null);
		return cliente ==null ? null: modelMapper.map(cliente, ClienteDto.class);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		clienteDao.deleteById(id);
	}

}
