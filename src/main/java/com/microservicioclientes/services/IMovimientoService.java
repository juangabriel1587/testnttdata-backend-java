package com.microservicioclientes.services;

import java.util.Date;
import java.util.List;

import com.microservicioclientes.dto.MovimientoDto;

public interface IMovimientoService {

	
	public List<MovimientoDto> findAll();
	
	public MovimientoDto findById(Long id);
	
	public MovimientoDto save(MovimientoDto movimiento);
	
	public void delete(Long id);
	
	public List<MovimientoDto> findByCuentaId(Long cuentaId,String fechadesde,String fechahasta);
	
	public Double valorDiario(Long cuentaId,String hoy);
	
	public MovimientoDto obtenerUltimoRegistro(Long cuentaId);
}
