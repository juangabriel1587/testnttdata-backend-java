package com.microservicioclientes.services;

import java.util.List;

import com.microservicioclientes.dto.CuentaDto;

public interface ICuentaService {
	
	public List<CuentaDto> findAll();
	
	public CuentaDto finfById(Long id);

	public CuentaDto save(CuentaDto cuenta);
	
	public void delete(Long id);
	
	public List<CuentaDto> finByClienteId(Long clienteId);
}
