package com.microservicioclientes.services;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.microservicioclientes.dto.MovimientoDto;
import com.microservicioclientes.models.dao.ICuentaDao;
import com.microservicioclientes.models.dao.IMovimientosDao;
import com.microservicioclientes.models.entity.Cuenta;
import com.microservicioclientes.models.entity.Movimientos;

@Service
public class MovimientoServiceImpl  implements IMovimientoService{

	@Autowired
	private IMovimientosDao movimientoDao;
	@Autowired
	private ICuentaDao cuentaDao;
	@Autowired
	private ModelMapper modelMapper;
	
	
	@Override
	@Transactional(readOnly = true)
	public List<MovimientoDto> findAll() {
		// TODO Auto-generated method stub
		List<Movimientos> movimientos= movimientoDao.findAll();
		// map dto list
		return movimientos.stream().map(mov->modelMapper.map(mov, MovimientoDto.class)).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public MovimientoDto findById(Long id) {
		// TODO Auto-generated method stub
		
		Movimientos movimiento = movimientoDao.findById(id).orElse( null);
		// map DTO
		return movimiento==null? null : modelMapper.map(movimiento, MovimientoDto.class);
	}

	@Override
	@Transactional
	public MovimientoDto save(MovimientoDto movimiento) {
		// TODO Auto-generated method stub
		
		
		
		Movimientos movn = new Movimientos();//movimientoDao.save(modelMapper.map(movimiento, Movimientos.class));
		Cuenta cuenta = cuentaDao.findById(movimiento.getCuentaId()).orElse(null);
		movn.setCuenta(cuenta);
		movn.setSaldo(movimiento.getSaldo());
		movn.setTipoMovimiento(movimiento.getTipoMovimiento());
		movn.setValor(movimiento.getValor());
		Movimientos mv= movimientoDao.save(movn);
		
		return modelMapper.map(mv, MovimientoDto.class);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		movimientoDao.deleteById(id);
	}

	@Override
	public List<MovimientoDto> findByCuentaId(Long cuentaId,String desde,String hasta) {
		// TODO Auto-generated method stub
		List<Movimientos> movimientos= movimientoDao.finByCuentaQuery(cuentaId,desde,hasta);
		// map dto list
		return movimientos.stream().map(mov->modelMapper.map(mov, MovimientoDto.class)).collect(Collectors.toList());
	}

	@Override
	public Double valorDiario(Long cuentaId, String hoy) {
		// TODO Auto-generated method stub
		
		Double valor = movimientoDao.finByLImiteHoyQuery(cuentaId, hoy);
		return valor==null?0:valor;
	}

	@Override
	public MovimientoDto obtenerUltimoRegistro(Long cuentaId) {
		// TODO Auto-generated method stub
		Movimientos movimiento = movimientoDao.finByLastQuery(cuentaId);
		// map DTO
		return movimiento==null? null :modelMapper.map(movimiento, MovimientoDto.class);
	}

}
