package com.microservicioclientes.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.microservicioclientes.models.entity.Cuenta;


public interface ICuentaDao extends JpaRepository<Cuenta, Long> {

	@Query(value = "select c.* from cuentas c where c.cliente_id=?1",nativeQuery=true)
	public List<Cuenta> finByCuentaQuery(Long clienteId);
}
