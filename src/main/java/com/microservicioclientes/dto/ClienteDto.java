package com.microservicioclientes.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonValue;





public class ClienteDto implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String nombre;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String genero;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String edad;
	@Size(min = 10,max = 13)
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String identificacion;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String direccion;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String telefono;
	
	private Date createdAt;
	private String contrasena;
	private String usuario;

	
	private Boolean estado;
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	
	
	



	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}







	public enum EstadoCliente{
		ACTIVO,
		INACTIVO;
	}
	
}
