package com.microservicioclientes.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.microservicioclientes.models.entity.Cliente;
import com.microservicioclientes.models.entity.Movimientos;




public class CuentaDto implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String numeroCuenta;
	@NotEmpty(message = "Es requedido")
	@NotNull(message = "No puede tener valores nulos")
	private String tipoCuenta;
	
	private Double saldoInicial;
	private Boolean estado;
	private Date createdAt;
	private Double limiteDiario;
	
	private Long clienteId;
	private ClienteDto cliente;
	private List<MovimientoDto> movimientos;
	
	
	
	
	
	public List<MovimientoDto> getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(List<MovimientoDto> movimientos) {
		this.movimientos = movimientos;
	}
	public Double getLimiteDiario() {
		return limiteDiario;
	}
	public void setLimiteDiario(Double limiteDiario) {
		this.limiteDiario = limiteDiario;
	}
	public ClienteDto getCliente() {
		return cliente;
	}
	public void setCliente(ClienteDto cliente) {
		this.cliente = cliente;
	}
	
	public Long getClienteId() {
		return clienteId;
	}
	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	
	
	public Double getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(Double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
}
